package A405_stringManipulation;

import java.util.Scanner;

public class StringManipulation {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Please enter a sentence: ");
		String sentence = sc.nextLine();
		System.out.println(sentence.toUpperCase());
		System.out.println(sentence.toLowerCase());
		System.out.println(sentence.replace('a', 'o'));
		System.out.println(sentence.length());
		System.out.println(sentence.charAt(0));
		StringBuilder sb = new StringBuilder(sentence);
		System.out.println(sb.reverse().charAt(0));
		int counter = 0;
		for (int i = 0; i < sentence.length(); i++) {
			if (sentence.charAt(i) == 'e') {
				counter++;
			}
		}
		System.out.println(counter);
	}
}

