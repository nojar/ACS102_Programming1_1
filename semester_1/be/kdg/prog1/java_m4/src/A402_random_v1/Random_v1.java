package A402_random_v1;

import java.util.Random;

public class Random_v1 {
	public static void main(String[] args) {
		Random random = new Random();
		System.out.println("six random numbers from 1 to 6 (both inclusive) on a single line");
		for (int i = 0; i < 6; i++) {
			System.out.print(random.nextInt(6) + 1 + " ");
		}
		System.out.println("\nfour random booleans on a single line");
		for (int i = 0; i < 4; i++) {
			System.out.print(random.nextBoolean() + " ");
		}
		System.out.println("\nPrint three random decimals from 0 to 1 (not including 1)");
		for (int i = 0; i < 3; i++) {
			System.out.print(random.nextFloat() + " ");
		}
		System.out.println("\n10 random numbers between 900 and 1000 (both inclusive)");
		for (int i = 0; i < 10; i++) {
			System.out.println(random.nextInt(1000) + 900 + " ");
		}
		System.out.println("\n10 random decimals between 5 and 10 (i.e. 6.481)");
		for (int i = 0; i < 10; i++) {
			System.out.println(random.nextDouble() + " ");
		}
	}
}
