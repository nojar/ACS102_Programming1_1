package A411_comparingString_extra;

import java.util.Scanner;

public class ComparingString_extra {

	public static void main(String[] args) {
		//Part 1
		System.out.println("--------Part 1-----------");
		String literal = "Harry";
		String newReference = "Harry";
		String newObject = new String("Harry");

		// Check if literal and newReference are the same.
		// Compare them using ==.
		if (literal == newReference) {
			System.out.println("True");
		} else {
			System.out.println("False");
		}

		// Check if literal and newObject are the same.
		// Compare them using ==.
		if (literal == newObject) {
			System.out.println("True");
		} else {
			System.out.println("False");
		}

		// Check if literal and newReference are the same.
		// Compare them using the 'equals' method of String.
		if (literal.equals(newReference)) {
			System.out.println("True");
		} else {
			System.out.println("False");
		}

		// Check if literal and newObject are the same.
		// Compare them using the 'equals' method of String.
		if (literal.equals(newObject)) {
			System.out.println("True");
		} else {
			System.out.println("False");
		}

		//Part 2
		System.out.println("---------Part 2-----------");
		String stringOne;
		String stringTwo;

		// Read both strings using a Scanner.
		Scanner sc = new Scanner(System.in);
		System.out.print("First string: ");
		stringOne = sc.nextLine();
		System.out.print("Second string: ");
		stringTwo = sc.nextLine();


		// Trim both strings.
		System.out.println(stringOne.trim() + " " + stringTwo.trim());

		//TODO
		// Compare them and print them in alphabetical order.

		//Part 3
		System.out.println("----------Part 3----------");
		String string_One = "java";
		String string_Two = "Java";
		String string_Three = "JAVA";

		if (string_One.equalsIgnoreCase(string_Two)) {
			System.out.println(string_One + " and " + string_Two + " are equal");
		} else {
			System.out.println(stringOne + " and " + stringTwo + " are not equal");
		}
		if (string_One.equalsIgnoreCase(string_Three)) {
			System.out.println(string_One + " and " + string_Three + " are equal");
		} else {
			System.out.println(string_One + " and " + string_Three + " are not equal");
		}
		if (string_Two.equalsIgnoreCase(string_Three)) {
			System.out.println(string_Two + " and " + string_Three + " are equal");
		} else {
			System.out.println(string_Two + " and " + string_Three + " are not equal");
		}


	}
}
