package A409_stringbuilder_v2_extra;

import java.util.Scanner;

public class StringBuilder_extra {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		StringBuilder sb = new StringBuilder();
		final int MAX = 20;
		System.out.print("Enter the highest number(1...20): ");
		if (!sc.hasNextInt()) {
			System.out.println("You didn't enter a number! ");
		} else {
			int number = sc.nextInt();
			if (number > 0 && number <= MAX) {
				for (int i = 1; i <= number; i++) {
					System.out.print(i + " ");
					sb.append(i);
				}
				System.out.println("\n" + sb);
			} else {
				System.out.println("The number should be between 1 and 20!");
			}
		}
	}
}
