package A406_stringBuilder_v1;

import java.util.Random;
import java.util.Scanner;

public class StringBuilder_v1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		StringBuilder builderOne;
		StringBuilder builderTwo;
		StringBuilder builderThree;
		StringBuilder builderFour;
		System.out.print("Enter your firstname and lastname: ");
		String name = sc.nextLine();
		builderOne = new StringBuilder();
		builderOne.append(name.charAt(0));
		for (int i = 0; i < name.length(); i++) {
			char c = name.charAt(i);
			if (c == ' ') {
				builderOne.append(name.charAt(i + 1));
			}
		}
		System.out.println(builderOne);

		builderTwo = new StringBuilder(name);
		System.out.println(builderTwo.reverse());
		builderThree = new StringBuilder(name.replaceAll("e", "a"));
		System.out.println(builderThree);
		builderFour = new StringBuilder(name);
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < builderFour.length(); i++) {
			Random random = new Random();
			int index = random.nextInt(builderFour.length());
			sb.append(builderFour.charAt(index));
			builderFour.deleteCharAt(index);// delete duplicates

		}
		System.out.println(sb.toString().toCharArray());
	}
}
