package A403_random_v2_extra;

import java.util.Random;

public class Random_v2_extra {
	public static void main(String[] args) {
		Random random = new Random();
		Random random1 = new Random(42);
		Random random2 = new Random(42);
		for (int i = 0; i < 5; i++) {
			System.out.printf("%.2f ", random.nextDouble());
		}
		System.out.println();
		for (int i = 0; i < 10; i++) {
			System.out.printf("%d ", random1.nextInt(42) + 1);
			System.out.printf("%d ", random2.nextInt(42) + 1);
		}
	}
}
