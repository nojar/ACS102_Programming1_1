package A209_pizza;
import java.util.Scanner;

public class Pizza {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int singlePizzaPrice = 800;
		final int toppingValue = 50;
		int terms = 1;
		int totalAmount2 = 0;
		int totalPrice = 0;
		System.out.print("How many pizza would you like to have? ");
		int numberOfPizza = sc.nextInt();
		int totalAmount1 = singlePizzaPrice * numberOfPizza;
		while (terms <= numberOfPizza) {
			System.out.print("How many extra toppings for pizza " + terms + "? ");
			int numberOfToppings = sc.nextInt();
			totalAmount2 += numberOfToppings * toppingValue;
			terms++;
			totalPrice = totalAmount1 + totalAmount2;
		}
		System.out.println("Total Price now is " + totalPrice / 100.0);
	}
}

