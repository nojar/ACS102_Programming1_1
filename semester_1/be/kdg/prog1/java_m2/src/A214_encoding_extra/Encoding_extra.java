package A214_encoding_extra;

import java.util.Scanner;

public class Encoding_extra {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a  message: ");
		String message = sc.nextLine().toUpperCase();
		System.out.print("Displacement to be used: ");
		int displacement = sc.nextInt();

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < message.length(); i++) {
			char encoding = (char) (message.charAt(i) + displacement);
			if (encoding > 'Z') {
				sb.append((char) (message.charAt(i) - 26 - displacement));
			} else {
				sb.append((char) (message.charAt(i) + displacement));
			}
		}
		System.out.println("Encoded Message: \n" + sb);
		System.out.println("Decoded message: \n" + message.toUpperCase());

	}
}
