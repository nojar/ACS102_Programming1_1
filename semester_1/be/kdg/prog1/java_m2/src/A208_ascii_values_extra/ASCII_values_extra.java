package A208_ascii_values_extra;

import java.util.Scanner;

public class ASCII_values_extra {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a string: ");
		String stringSentence = sc.nextLine();
		int charValue ;
		for (char c : stringSentence.toCharArray()) {
			charValue = c;
			System.out.println(c + " has an ASCII value of " + charValue);
		}
	}
}
