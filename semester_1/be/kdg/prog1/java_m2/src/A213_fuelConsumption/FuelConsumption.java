package A213_fuelConsumption;

import java.util.Scanner;

public class FuelConsumption {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the previous mileage: ");
		int previousMileage = sc.nextInt();
		System.out.print("Enter the current mileage: ");
		int currentMileage = sc.nextInt();
		System.out.print("Enter the amount of liters refilled: ");
		double amountOfLitersRefilled = sc.nextDouble();
		int mileage = currentMileage - previousMileage;
		double consumption = amountOfLitersRefilled/mileage;
		System.out.printf("Consumption for %dkm driven: %.2f litres/100km", mileage, consumption*100);
	}
}
