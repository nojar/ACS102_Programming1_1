package A215_switches_extra;

import java.util.Scanner;

public class Switches_extra {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
			System.out.print("Enter the state of switch 1 (true or false):");
			Boolean switch1 = sc.nextBoolean();
			System.out.print("Enter the state of switch 2 (true or false):");
			Boolean switch2 = sc.nextBoolean();
			System.out.print("Enter the state of switch 3 (true or false):");
			Boolean switch3 = sc.nextBoolean();

			if (switch1 & switch2 & switch3) {
				System.out.println("At least two switches are turned on.");
			} else if (switch1 & switch2 | switch1 & switch3 | switch2 & switch3) {
				System.out.println("At least two switches are turned on.\nExactly two switches are turned on.");
			} else {
				System.out.println("At most one switch is turned on");
			}
		}

}
