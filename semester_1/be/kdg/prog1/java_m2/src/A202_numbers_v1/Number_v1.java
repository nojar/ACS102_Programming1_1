package A202_numbers_v1;

public class Number_v1 {
	public static void main(String[] args) {
		//Part 1
		int i = 2000000000;
		int j = 2000000000;
		int sum = i + j;
		long sum1 = ((long) i + j);
		System.out.println(i + " + " + j + " = " + sum);
		System.out.println(i + " + " + j + " = " + sum1);

		//Part 2
		long firstNumber = 10000;
		long secondNumber = 10000;
		long results = firstNumber + secondNumber;
		System.out.println(results);

		//Part 3
		int first = 8;
		int second = 5;
		System.out.println("Sum : " + (first + second));
		System.out.println("difference : " + (first - second));
		System.out.println("product : " + (first * second));
		System.out.println("quotient : " + (first / second));
		System.out.println("reminder : " + (first % second));

		//Part 4
		System.out.println("++first : " + (++first)); // adds 1 immediately to the value
		System.out.println("first++ : " + (first++)); // add 1 to the value after the first run
		System.out.println("--second : " + (--second)); //subtract 1 immediately from the value
		System.out.println("second-- : " + (second--)); //subtract 1 from the value after the first run
	}
}
