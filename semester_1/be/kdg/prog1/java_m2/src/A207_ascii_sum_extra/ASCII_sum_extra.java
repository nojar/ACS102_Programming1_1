package A207_ascii_sum_extra;

import java.util.Scanner;

public class ASCII_sum_extra {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a string :");
		String sentence = sc.nextLine();
		int sum = 0;
		for (char c : sentence.toCharArray()) {
			sum += c;

		}
		System.out.println("Sum = " + sum);

	}
}
