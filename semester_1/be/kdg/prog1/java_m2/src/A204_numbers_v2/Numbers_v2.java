package A204_numbers_v2;

import jdk.swing.interop.SwingInterOpUtils;

import java.util.Scanner;

public class Numbers_v2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		final long MINIMUM = 100000L;
		final long MAXIMUM = 999999L;
		long product;
		System.out.print("Please enter a 6-digits number: ");
		long number = sc.nextLong();
		System.out.print("Please enter another 6-digits number: ");
		long anotherNumber = sc.nextLong();
		if (number < MINIMUM || anotherNumber < MINIMUM) {
			System.out.println("One of the number is too small");
		} else if (number > MAXIMUM || anotherNumber > MAXIMUM) {
			System.out.println("One of the number is too big");
		} else {
			product = number * anotherNumber;
			System.out.println("The product is " + product);
			System.out.println("The final 5-digits are "+ product % MINIMUM);
		}

	}
}
