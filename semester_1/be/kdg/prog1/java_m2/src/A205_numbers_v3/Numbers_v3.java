package A205_numbers_v3;

import java.util.Scanner;

public class Numbers_v3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		final long MINIMUM_DIVIDEND = 1000000000000L;
		final long MINIMUM_DIVISOR = 10000000L;
		long quotient;
		System.out.print("Enter a 13-digits whole number: ");
		long dividend = sc.nextLong();
		System.out.print("Enter a 8-digits whole number: ");
		long divisor = sc.nextLong();
		if (dividend < MINIMUM_DIVIDEND || divisor < MINIMUM_DIVISOR) {
			System.out.println("One of the number is too small");
		} else {
			quotient = dividend / divisor;
			System.out.println("The quotient is " + quotient);
		}

	}
}
