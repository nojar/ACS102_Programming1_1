package A211_digits_v2;

import java.util.Scanner;

public class Digits_v2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		long number;
		long sum = 0L;
		while (true) {
			System.out.print("Enter a whole number: ");
			long wholeNumber =sc.nextLong();
			String newWholeNumber = String.valueOf(wholeNumber);
			if (wholeNumber == -1) {
				return;
			} else {
				for (int i = 0; i < newWholeNumber.length(); i++) {
					number = Integer.parseInt(String.valueOf(newWholeNumber.charAt(i)));
					sum += number ;
				}
				System.out.println("The sum of the digits of this number is "+sum);
			}
		}
	}
}
