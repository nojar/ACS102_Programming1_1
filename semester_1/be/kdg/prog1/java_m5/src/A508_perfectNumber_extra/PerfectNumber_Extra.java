package A508_perfectNumber_extra;

/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 18/10/2021
 */
public class PerfectNumber_Extra {
	public static String getPerfectNumber(int number) {
		StringBuilder sb = new StringBuilder();
		int factors = 0;
		int sum = 0;
		for (int i = 0; i < number; i++) {
			factors++;
			if (number % factors == 0 && factors != number) {
				sum += factors;
				sb.append(factors).append(" ");

				if (sum == number) {
					System.out.print(number + " --> ");
					System.out.println(sb);
				}
			}
		}
		return "";
	}
}

