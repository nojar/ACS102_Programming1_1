package A504_operations;

import java.util.Scanner;
/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 18/10/2021
 */
public class DemoOperations {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Please enter the first integer: ");
		int userInput1 = scanner.nextInt();
		System.out.print("Please enter the second integer: ");
		int userInput2 = scanner.nextInt();
		Operations operations = new Operations(userInput1, userInput2);
		operations.sum();
		operations.difference();
		operations.product();
		operations.quotient();
	}
}
