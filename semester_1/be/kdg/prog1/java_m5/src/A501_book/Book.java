package A501_book;
/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 18/10/2021
 */

public class Book {
	private String author;
	private String title;
	private int numberOfPages;
	private boolean onLoan;

	public Book(String author, String title, int numberOfPages) {
		this.author = author;
		this.title = title;
		this.numberOfPages = numberOfPages;
	}

	public Book() {
		new Book("unknown", "unknown", 0);
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}

	public boolean isOnLoan() {
		onLoan = getNumberOfPages() < 600 || getNumberOfPages() > 700;
		return onLoan;
	}

	public void setOnLoan(boolean onLoan) {
		this.onLoan = onLoan;
	}

	@Override
	public String toString() {
		return "Book " + title.toUpperCase()
				+ "(" + numberOfPages + " pages), written by " + author.toUpperCase()
				+ " is  "+ (onLoan ? "on loan." : "available.");
	}
	}
