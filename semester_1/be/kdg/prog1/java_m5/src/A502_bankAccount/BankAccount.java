package A502_bankAccount;
/*
 * @author: RANSFORD ADDAI
 *
 * */
public class BankAccount {
	private String holder;
	private String iban;
	private double balance;

	public BankAccount(String holder, String iban, double balance) {
		this.holder = holder;
		this.iban = iban;
		this.balance = balance;
	}

	public BankAccount(String holder, String iban) {
		this.holder = holder;
		this.iban = iban;
		this.balance = 0;

	}

	public String getHolder() {
		return holder;
	}

	public String getIban() {
		return iban;
	}

	public double getBalance() {
		return balance;
	}

	public void deposit(double amount) {
		balance = getBalance() + amount;
	}

	public boolean withdraw(double amount) {
		if (getBalance() < 0 || getBalance() - amount < 0) {
			return false;
		} else {
			this.balance = getBalance() - amount;
			return true;
		}
	}

	public String toString() {
		return "The account " + getIban() + " of " + getHolder() + " has a balance of £" + getBalance();
	}
}
