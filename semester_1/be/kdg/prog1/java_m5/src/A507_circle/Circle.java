package A507_circle;

/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 18/10/2021
 */
public class Circle {
	int radius;
	String color = "black";

	public Circle(int radius, String color) {
		this.radius = radius;
		this.color = color;
	}

	public Circle(int radius) {
		this.radius = radius;
	}

	public double circumference() {
		return (2 * Math.PI * getRadius());
	}

	public double surface() {
		return Math.PI * (getRadius() * getRadius());
	}

	public double deltaCircumference(Circle other) {
		return Math.abs(other.circumference() - this.circumference());
	}

	public double deltaSurface(Circle other) {
		return Math.abs(other.surface() - this.surface());
	}

	public int getRadius() {
		return radius;
	}

	public String getColor() {
		return color;
	}

	@Override
	public String toString() {
		return String.format(
				"Color: %s\nRadius: %d\nCircumference: %.2f\nSurface: %.2f"
				, getColor(), getRadius(), circumference(), surface());
	}
}
