package A503_box;

/*
 * @author: RANSFORD ADDAI
 * Monday 18/10/2021
 * */
public class Box {
	private final String type;
	private final double length;
	private final double width;
	private final double height;


	public Box(String type, double length, double width, double height) {
		this.type = type;
		this.length = length;
		this.width = width;
		this.height = height;
	}

	public Box(String type, double dimension) {
		this(type, dimension, dimension, dimension);
	}


	public double surface() {
		return ((2 * (length * width)) + (2 * (length * height)) + (2 * (height * width)));
	}

	public double volume() {
		return (width * length * height);
	}

	public double tapeLength() {
		return ((width * 2) + (height * 2));
	}

	public String getType() {
		return type;
	}

	public double getLength() {
		return length;
	}

	public double getWidth() {
		return width;
	}

	public double getHeight() {
		return height;
	}

	@Override
	public String toString() {
		return "Type: " + getType() + "\n" +
				"Length: " + getLength() + "\n" +
				"Width: " + getWidth() + "\n" +
				"Height: " + getHeight() + "\n" +
				"Surface: " + surface() + "\n" +
				"Volume: " + volume() + "\n" +
				"Minimum tapeLength: " + tapeLength() + "\n"

				;
	}
}

