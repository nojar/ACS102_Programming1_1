package A606_countingLetters;

import java.util.Scanner;

public class CountingLetters {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Please enter a sentence: ");
		String sentence = sc.nextLine().toLowerCase();
		int[] frequencies = new int[26];
		char[] alphabet = new char[26];
		int terms = 0;

		char c = 'a';
		for (int i = 0; i < 26; i++) {
			alphabet[i] = c;
			c++;
		}

		for (int i = 0; i < sentence.length(); i++) {
			if (Character.isAlphabetic(sentence.charAt(i))) {
				for (char letter : alphabet) {
					if (sentence.charAt(i) == letter) {
						frequencies[letter - 97]++;
						terms++;
					}
				}
			} else {
				System.out.printf("The character %c is not an alphabet.\n", sentence.charAt(i));
			}
		}
		System.out.println("Letter frequencies:");
		for (int i = 0; i < frequencies.length; i++) {
			System.out.printf("%c --> %d times  ", i + 97, frequencies[i]);
			if ((i + 1) % 5 == 0) {
				System.out.println();
			}
		}
		System.out.println("\nTotal amount of letters: " + terms);
	}
}
