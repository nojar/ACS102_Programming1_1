package A603_forEach;

import java.util.Arrays;

/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 25/10/2021
 */
public class ForEach {
	public static void main(String[] args) {
		int[] myArray = new int[20];
		int multiplesOfSeven = 0;
		for (int i = 0; i < 20; i++) {
			multiplesOfSeven += 7;
			myArray[i] = multiplesOfSeven;
		}
		for (int i : myArray) {
			System.out.printf("%d ", i);
		}
		System.out.println("\nReverse Array");
		for (int i = myArray.length - 1; i >= 0; i--) {
			System.out.printf("%d ",myArray[i] );
		}
	}
}
