package A611_matrix;

/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 25/10/2021 16:56
 */
public class Matrix {
	public static void main(String[] args) {
		int[][] matrix = new int[4][6];
		for (int rowNumber = 0; rowNumber < matrix.length; rowNumber++) {
			for (int columnNumber = 0; columnNumber < matrix[rowNumber].length; columnNumber++) {
				matrix[rowNumber][columnNumber] = (rowNumber + 1) * (columnNumber + 1);
			}
		}
		for (int[] row : matrix) {
			for (int value : row) {
				System.out.printf("%2d ", value);
			}
			System.out.println();
		}
	}
}
