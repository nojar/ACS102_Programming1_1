package A609_arrayOfCharacters;

/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 24/10/2021
 */
public class ArrayOfCharacters {
	public static void main(String[] args) {
		String word = "JavaScript";
		char[] letters = word.toCharArray();
		for (char letter : letters) {
			System.out.print(letter + " ");
		}
	}
}
