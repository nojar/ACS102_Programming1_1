package A608_split;

import jdk.swing.interop.SwingInterOpUtils;

import java.util.Arrays;

/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 25/10/2021
 */
public class Split {
	public static void main(String[] args) {
		String text = "Java can be tricky at times";
		String[] textArray = text.split("");
		for (String s : textArray) {
//			System.out.print("\"" + s + "\"");
			System.out.println(s);

		}
	}
}
