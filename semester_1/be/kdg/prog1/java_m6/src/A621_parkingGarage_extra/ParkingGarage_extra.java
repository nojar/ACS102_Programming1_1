package A621_parkingGarage_extra;

import java.util.Arrays;

/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 25/10/2021
 */
public class ParkingGarage_extra {
	int numberOfGaragesAvailable = 2;
	int numberOfFloorsPerGarage = 2;
	int parkingSpot = 15;
	int[][][] parkingSpots = new int[numberOfGaragesAvailable][numberOfFloorsPerGarage][parkingSpot];

	//this class is not yet complete
	public static void main(String[] args) {
	}
}
