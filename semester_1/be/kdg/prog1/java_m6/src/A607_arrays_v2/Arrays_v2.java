package A607_arrays_v2;

/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 25/10/2021
 */
public class Arrays_v2 {
	public static void main(String[] args) {
		StringBuilder[] suits = new StringBuilder[4];
		suits = new StringBuilder[]{new StringBuilder("hearts"), new StringBuilder("clubs"), new StringBuilder("diamonds"), new StringBuilder("spades")};
		for (StringBuilder s : suits) {
			System.out.println(s);
		}
		StringBuilder[] mySuits = {new StringBuilder("hearts"), new StringBuilder("clubs"), new StringBuilder("diamonds"), new StringBuilder("spades")};
		for (StringBuilder s : mySuits) {
			System.out.println(s);
		}
	}
}
