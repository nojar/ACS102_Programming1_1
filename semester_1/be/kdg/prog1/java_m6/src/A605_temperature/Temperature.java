package A605_temperature;

import java.util.Scanner;

/**
 * @author RANSFORD ADDAI
 * ransford.addai@student.kdg.be
 * Monday 25/10/2021
 */
public class Temperature {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		final int NUMBER_OF_TEMPERATURES = 7;
		double[] tempArray = new double[NUMBER_OF_TEMPERATURES];
		System.out.printf("Please enter %d temperatures\n", NUMBER_OF_TEMPERATURES);
		for (int i = 0; i < NUMBER_OF_TEMPERATURES; i++) {
			System.out.printf("Day %d: ", i + 1);
			double temp = sc.nextDouble();
			tempArray[i] = temp;
		}
		System.out.println("Summary: \n=================");
		int terms = 1;
		double sum = 0;
		for (double i : tempArray) {
			sum += i;
			System.out.printf("Day %d: %10.2f\n", terms, i);
			terms++;
		}
		System.out.println("=================");
		double average = sum / tempArray.length;
		System.out.printf("Average: %.2f", average);
	}
}
