package A04_welcome;

import java.util.Scanner;

public class Welcome {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("What is your name?");
		String name = sc.nextLine();
		System.out.println("Welcome " + name + "!");
	}
}
