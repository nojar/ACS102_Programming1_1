package A14_multiplicationTables;

import java.util.Scanner;

public class MultiplicationTables {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		final int length = 10;
		System.out.print("Which multiplication table would you like to see? ");
		int number = sc.nextInt();
		for (int i = 1; i <= length; i++) {
			int product = i * number;
			System.out.print(i + " * " + number + " = ");
			int yourAnswer = sc.nextInt();
			if (yourAnswer == product) {
				System.out.println("Correct");
			} else {
				System.out.println("Wrong");
			}
		}
	}
}