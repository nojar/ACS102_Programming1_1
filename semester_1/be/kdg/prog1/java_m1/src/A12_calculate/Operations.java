package A12_calculate;

import java.util.Scanner;

public class Operations {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int answer ;
		System.out.print("Enter a number: ");
		int number = sc.nextInt();
		System.out.print("Enter another number: ");
		int anotherNumber = sc.nextInt();
		System.out.println("Choose an operation");
		System.out.println("1. add");
		System.out.println("2. subtract");
		System.out.println("3. multiply");
		System.out.println("4. divide");
		System.out.println("5. exponent");
		int operator = sc.nextInt();
		if (operator == 1) {
			answer = number + anotherNumber;
			System.out.println("Your choice: " + operator);
			System.out.println(number + " + " + anotherNumber + " = " + answer);

		} else if (operator == 2) {
			answer = number - anotherNumber;
			System.out.println("Your choice: " + operator);
			System.out.println(number + " - " + anotherNumber + " = " + answer);

		} else if (operator == 3) {
			answer = number * anotherNumber;
			System.out.println("Your choice: " + operator);
			System.out.println(number + " * " + anotherNumber + " = " + answer);

		} else if (operator == 4) {
			answer = number / anotherNumber;
			System.out.println("Your choice: " + operator);
			System.out.println(number + " / " + anotherNumber + " = " + answer);

		} else if (operator == 5) {
			answer =1;
			for (int i = 0; i < anotherNumber; i++) {
				answer *= number;
			}
			System.out.println("Your choice: " + operator);
			System.out.println(number + " ^ " + anotherNumber + " = " + answer);

		}

	}

}
