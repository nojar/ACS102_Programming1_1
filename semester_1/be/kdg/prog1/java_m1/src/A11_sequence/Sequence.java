package A11_sequence;

import java.util.Scanner;

public class Sequence {
	public static void main(String[] args) {
		int counter = 1;
		Scanner sc = new Scanner(System.in);
		System.out.print("How many numbers do you want to print? ");
		int number = sc.nextInt();
		System.out.print("What is the starting value? ");
		int value = sc.nextInt();
		System.out.print("What is the increment? ");
		int increment = sc.nextInt();
		while (counter < number) {
			counter++;
			System.out.print(value + " ");
			value += increment;
		}


	}
}
