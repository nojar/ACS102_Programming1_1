package A06_age;

import java.time.LocalDate;
import java.util.Scanner;

public class Age {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Please enter your name: ");
		String name = sc.nextLine();
		System.out.print("Dear " + name + ", please enter the year you were born: ");
		int year = sc.nextInt();
		int age = LocalDate.now().getYear() - year;
		System.out.println("If all goes well you'll be " + age + " by the end of the year");

	}
}

