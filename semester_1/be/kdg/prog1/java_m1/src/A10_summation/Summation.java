package A10_summation;

import java.util.Scanner;

public class Summation {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int stop = 0;
		int sum = 0;
		int counter = -1;
		while (true) {
			System.out.print("Enter a number(to stop enter 0): ");
			int number = sc.nextInt();
			sum += number;
			counter++;
			if (number == stop) {
				System.out.printf("You enter %d numbers\n", counter);
				System.out.println("The sum of these numbers is " + sum);
				return;
			}
		}
	}
}
