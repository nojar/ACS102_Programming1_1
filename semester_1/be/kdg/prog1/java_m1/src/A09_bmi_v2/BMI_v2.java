package A09_bmi_v2;

import java.util.Scanner;

public class BMI_v2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Dear patient, this program will calculate your BMI");
		System.out.print("Enter your weight in kilograms: ");
		double weight = sc.nextDouble();
		System.out.print("Enter your length in meters: ");
		double length = sc.nextDouble();
		double bmi = (weight / (length * length));
		System.out.println("Your BMI is : " + bmi);
		if (bmi < 18) {
			System.out.println("Underweight");
		} else if (bmi > 18 & bmi > 25) {
			System.out.println("Healthy weight");
		} else if (bmi >= 25 & bmi < 30) {
			System.out.println("Overweight");
		} else {
			System.out.println("obese");
		}

	}
}
