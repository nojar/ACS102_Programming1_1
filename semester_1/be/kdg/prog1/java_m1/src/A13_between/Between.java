package A13_between;

import java.util.Scanner;

public class Between {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter a number (1 to 100)");
		while (true) {
			System.out.print("Enter the first number: ");
			int i = scanner.nextInt();
			if (i < 1 || i > 100) {
				System.out.print("Enter a number within the range (1-100)");
			} else {
				System.out.print("Enter the second number: ");
				int j = scanner.nextInt();
				if (j < 1 || j > 100) {
					System.out.print("Enter a number within the range (1-100)");
				} else {
					System.out.print("Enter the third number: ");
					int k = scanner.nextInt();
					if (k < 1 || k > 100) {
						System.out.print("Enter a number within the range (1-100)");
					} else {
						//Checking if i is middle number or not
						if (i < j && i > k || i > j && i < k) {
							System.out.println("The middle number is " + i);
						}
						//Checking if j is middle number or not
						if (j < i && j > k || j > i && j < k) {
							System.out.println("The middle number is " + j);
						}
						//Checking if k is middle number or not
						if (k < j && k > i || k > j && k < i) {
							System.out.println("The middle number is " + k);
						}
					}
				}
			}
			//allows the user to continue
			System.out.print("\nWould you like to continue? y/n");
			char c = scanner.next().toLowerCase().charAt(0);
			if (c == 'y') {
				continue;
			} else if (c == 'n')
				System.out.print("Program terminated");
			return;
		}
	}
}
