package A311_multiplicationTable_extra;

import java.util.Scanner;

public class MultiplicationTable_Extra {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.print("Enter a number between 1 and 30: ");
			int number = sc.nextInt();
			if (number >= 1 && number <= 30) {
				for (int i = 1; i <= number; i++) {
					for (int j = 1; j <= number; j++) {
						System.out.printf("|%3s ", (i * j));
					}
					System.out.println();
				}
			} else {
				System.out.println("invalid number");
				return;
			}
		}
	}
}
