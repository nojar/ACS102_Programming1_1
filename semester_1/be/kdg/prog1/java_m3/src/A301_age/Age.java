package A301_age;

import java.util.Scanner;

public class Age {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter your age: ");
		int age = sc.nextInt();
		if (age <2) {
			System.out.println("Baby");
		} else if (age <= 12) {
			System.out.println("Child");
		} else if (age <= 17) {
			System.out.println("Teenage");
		} else {
			System.out.println("Adult ");
		}
	}
}
