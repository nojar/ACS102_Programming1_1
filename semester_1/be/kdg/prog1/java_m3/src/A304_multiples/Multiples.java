package A304_multiples;

import java.util.Scanner;

public class Multiples {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a number to display it's multiples: ");
		int number = sc.nextInt();
		for (int i = number; i < 100; i += number) {
			System.out.println(i);
		}
	}
}
