package A308_divisible;

import java.util.Scanner;

public class Divisible {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.print("Enter a first divider (end the program with 0): ");
			int firstDivider = sc.nextInt();
			System.out.print("Enter a second divider: ");
			int secondDivider = sc.nextInt();
			if (firstDivider < 0 || secondDivider < 0) {
				System.out.println("Please enter positive numbers");
			} else if (firstDivider == 0) {
				return;
			} else {
				int counter = -1;
				for (int i = 1; i < 1000; i++) {
					if (i % firstDivider == 0 && i % secondDivider == 0) {
						counter++;
						if (counter % 10 == 0) {
							System.out.println();
						}
						System.out.printf("%5d ",i);
					}
				}
				System.out.println();
			}
		}
	}
}
