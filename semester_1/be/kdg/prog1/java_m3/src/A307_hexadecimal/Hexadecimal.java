package A307_hexadecimal;

public class Hexadecimal {
	public static void main(String[] args) {
		//This program  prints all hexadecimal numbers:
		char letters = 'A';
		for (int i = 0; i <= 15; i++) {
			if (i >= 10) {
				System.out.print(letters + " ");
				letters++;
			} else {
				System.out.print(i + " ");
			}
		}
	}
}
