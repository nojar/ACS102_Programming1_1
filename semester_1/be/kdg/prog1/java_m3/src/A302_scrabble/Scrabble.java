package A302_scrabble;

import java.util.Scanner;

public class Scrabble {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		char letter;
		System.out.println("Each letter has some points try to get the higher one");
		System.out.print("Please enter a letter: ");
		letter = sc.nextLine().toUpperCase().charAt(0);

		switch (letter) {
			case 'A', 'E', 'I', 'O', 'U', 'L', 'N', 'S', 'T', 'R' -> System.out.print("1 Point");
			case 'D', 'G' -> System.out.print("2 Points");
			case 'B', 'C', 'M', 'P' -> System.out.print("3 Points");
			case 'F', 'H', 'V', 'W', 'Y' -> System.out.print("4 Points");
			case 'K' -> System.out.print("5 Points");
			case 'J', 'X' -> System.out.print("8 Points");
			case 'Q', 'Z' -> System.out.print("10 Points");
			default -> System.out.println();
		}
	}
}
