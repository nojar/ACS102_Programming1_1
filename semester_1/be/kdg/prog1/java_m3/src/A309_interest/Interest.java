package A309_interest;

import java.util.Scanner;

public class Interest {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double capital;
		System.out.print("Enter a starting capital in £ : ");
		double startingValue = sc.nextDouble();

		System.out.print("Enter the interest rate: ");
		double interestRate = sc.nextDouble();

		System.out.print("Enter the number of years: ");
		int numberOfYears = sc.nextInt();

		capital = startingValue * Math.pow((1 + (interestRate / 100)), numberOfYears);
		System.out.printf("The capital will amount to %d",(long) capital);
	}
}
