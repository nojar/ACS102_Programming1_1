package A303_loops;

public class Loops {
	public static void main(String[] args) {
		// Using For Loops
		System.out.println("Displays a line with all numbers from 120 to 100 (descending)");
		for (int i = 120; i > 99; i--) {
			System.out.print(i + " ");
		}
		System.out.println("\nDisplays a line with all multiples of 3 below 50");
		for (int i = 3; i < 50; i += 3) {
			System.out.print(i + " ");
		}
		System.out.println("\nDisplays a line with the powers of 5 below 10,000");
		for (int i = 5; i < 10000; i *= 5) {
			System.out.print(i + " ");
		}
		System.out.println("\nDisplays a line with the alphabet");
		int counter = 0;
		for (char i = 'A'; i <= 'Z'; i++) {
			counter++;
			if (counter % 10 == 0) {
				System.out.println();
			}
			System.out.print(i + " ");
		}
	}
}
