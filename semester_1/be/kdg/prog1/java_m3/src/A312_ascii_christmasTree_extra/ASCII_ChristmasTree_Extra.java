package A312_ascii_christmasTree_extra;

import java.util.Scanner;

public class ASCII_ChristmasTree_Extra {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		final int minWidth = 13;
		final int maxWidth = 29;
		final int truckWidth = 3;
		final int truckHeight = 4;

		System.out.println("The width should be an uneven number between 13-29!");
		System.out.print("Enter a width of the tree: ");
		int treeWidth = sc.nextInt();

		//checking if the user entered the right number
		if (treeWidth >= minWidth && treeWidth <= maxWidth) {
			if (treeWidth % 2 == 0) {
				treeWidth -= 1;
			}
		} else if (treeWidth < minWidth) {
			System.out.println(treeWidth = minWidth);
		} else {
			System.out.println(treeWidth = maxWidth);
		}
		System.out.println(treeWidth);

		int height = (treeWidth / 2) + truckHeight;
		for (int amountOfStars = 1; amountOfStars <= (height * 2) + 1; amountOfStars += 2) {
			if (amountOfStars <= treeWidth) {
				for (int i = 0; i < (treeWidth - amountOfStars) / 2; i++) {
					System.out.print(" ");
				}
					for (int stars = 0; stars < amountOfStars; stars++) {
						System.out.print("*");
					}
			} else {
				for (int i = 0; i < (treeWidth - truckWidth) / 2; i++) {
					System.out.print(" ");
				}
				for (int j = 0; j < truckHeight; j++) {
					System.out.print("*");
				}
			}
			System.out.println();
		}
	}
}
