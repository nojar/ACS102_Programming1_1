package A310_ascii_box_extra;

import java.util.Scanner;

public class ASCII_Box_Extra {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		do {
			System.out.println("We'll draw an ASCII box using a character and dimensions of your choice.");
			System.out.println("Select '1'or '2' or '3'to \n1 => Draw a filled box.\n2 => Draw a box outline.\n3 => To Quit");
			int option = sc.nextInt();
			System.out.print("Enter a Character: ");
			sc.nextLine();
			String character = sc.nextLine();

			System.out.print("Enter a width(2..60): ");
			int width = sc.nextInt();
			System.out.print("Enter a height(2..20): ");
			int height = sc.nextInt();
			if (width < 2 || width > 60) {
				System.out.println("Invalid width");
			} else if (height < 2 || height > 20) {
				System.out.println("Invalid height");

			}
			if (option == 1) {
				for (int i = 0; i < height; i++) {
					for (int j = 0; j < width; j++) {
						System.out.print(character);
					}
					System.out.println();
				}
			} else if (option == 2) {
				String s = character;
				for (int i = 0; i < height; i++) {
					for (int j = 0; j < width; j++) {
						if (i == 0 || i == height - 1 || j == width - 1) {
							character = s;
						} else if (j >= 1 && j <= width - 1) {
							character = " ";
						}
						System.out.print(character);
					}
					System.out.println();
				}
			} else if (option == 3) {
				break;
			} else {
					System.out.println("Select the right input!");
				return;
			}
		} while (true);
	}
}
