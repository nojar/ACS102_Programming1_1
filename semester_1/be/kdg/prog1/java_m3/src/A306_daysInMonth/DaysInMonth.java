package A306_daysInMonth;

import java.util.Scanner;

public class DaysInMonth {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a month number (1 = january, 2 = february....): ");
		int monthName = sc.nextInt();
		System.out.println("Enter a year(4 digits): ");
		int year = sc.nextInt();

		int days;
		String month;
		switch (monthName) {
			case 1 -> {
				month = "January";
				days = 31;
			}
			case 2 -> {
				month = "February";
				if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
					days = 29;
				} else {
					days = 28;
				}
			}
			case 3 -> {
				month = "March";
				days = 31;
			}
			case 4 -> {
				month = "April";
				days = 31;
			}
			case 5 -> {
				month = "May";
				days = 31;
			}
			case 6 -> {
				month = "June";
				days = 30;
			}
			case 7 -> {
				month = "July";
				days = 31;
			}
			case 8 -> {
				month = "August";
				days = 31;
			}
			case 9 -> {
				month = "September";
				days = 31;
			}
			case 10 -> {
				month = "October";
				days = 31;
			}
			case 11 -> {
				month = "November";
				days = 30;
			}
			case 12 -> {
				month = " December";
				days = 31;
			}
			default -> {
				System.out.println("Wrong month number ");
				month = "invalid";
				days = 0;
			}
		}
		System.out.print("In "+year + ", " + month + " has " + days + " days.");
	}
}

